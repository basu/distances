import distances
def test_euclidean():
    u = (2, -1)
    v = (-2, 2)
    assert distances.euclidean_distance(u, v) == 5