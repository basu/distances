# Distances

This is a python package for calculating the various distance matrix

# Installation

The project can be cloned locally using th command:

'''shell
git clone https://gitlab.gwdg.de/c.fortmanngrote/distances.git

# Usage
Currently the following distances measures are implemented in the package:

* Euclidean distances

# Testing
The package is tested with pytest. Run

'''shell
pytest test_euclidean.py

# License
The package is licensed under the term of the [MIT License] (https:/opensourse.org/licenses/MIT)

## TODOs
- [ ] Add more distances